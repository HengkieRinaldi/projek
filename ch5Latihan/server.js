// var
const { json } = require('express')
const express = require('express')
const app = express()
const port = 7777
const morgan = require('morgan')
const Routes = require('./Routes/Routes')

// middleware
app.use(morgan('dev'))
app.use(express.static('public'))
app.set('view engine', 'ejs')

// fungsi
app.use(Routes)

app.listen(port, function () {
    console.log(`server is running in port ${port}`)
})