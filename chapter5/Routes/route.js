const { json } = require('express')
const express = require('express')
const Routes = express.Router()
let userData = require('../public/user')
let dataAdmin = userData.admin

// landing
Routes.get('/', (req, res) => {
    res.render('index')

})
Routes.post('/', (req, res) => {
    res.send({
        message: 'login berhasil',
        resultData: dataAdmin[0],
        statusCode: 200
    })
})

// login
Routes.get('/login', (req, res) => {
    res.render('login')

})

Routes.post('/login', (req, res) => {
    let reqData = req.body
    if (reqData.username == dataAdmin[0].username) {
        if (reqData.password == dataAdmin[0].password) {
            res.send({
                message: 'login berhasil',
                resultData: dataAdmin[0],
                statusCode: 200
            })
        } else {
            res.send({ message: 'gagal login salah password' })
        }
    } else {
        res.send({ message: 'gagal login salah username' })
    }
})

// game
Routes.get('/game', (req, res) => {
    res.render('game')
})
Routes.post('/game', (req, res) => {
    res.send({
        message: 'login berhasil',
        resultData: dataAdmin[0],
        statusCode: 200
    })
})

//testing
Routes.get('/user-api', (req, res) => {
    res.send('minta data user')
})

Routes.post('/user-api', (req, res) => {
    res.send({
        message: 'okee',
        status: 200,
        data: dataAdmin
    })
})

module.exports = Routes