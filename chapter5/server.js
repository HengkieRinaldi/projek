// package
const fs = require('fs')
const express = require('express')
// const expressLayouts = require(express-ejs-layouts)
const app = express()
const morgan = require('morgan')
const port = 7000
const Routes = require('./Routes/route')

// middleware
app.use(morgan('dev'))
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.json())
app.use(Routes)

//nyalain port
app.listen(port, function(){
    console.log(`server nyala di port ${port}`)
})